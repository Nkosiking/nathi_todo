package com.nattysoft.nathi_todo

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import com.nattysoft.nathi_task.TaskAdapter
import com.nattysoft.nathi_todo.data.local.TaskListDatabase
import com.nattysoft.nathi_todo.data.local.models.Task
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.task_item_view.view.*


class MainActivity : AppCompatActivity(), TaskAdapter.OnTaskItemClickedListener{
    
    private var taskDatabase: TaskListDatabase? = null
    private var taskAdapter: TaskAdapter? = null
    var completedCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        taskDatabase = TaskListDatabase.getInstance(this)
        taskAdapter = TaskAdapter()
        taskAdapter?.setTaskItemClickedListener(this)

        add_task.setOnClickListener { startActivity(Intent(this, AddTaskActivity::class.java)) }

    }


    override fun onResume() {
        super.onResume()
        taskAdapter?.taskList=taskDatabase?.getTaskDao()?.getTaskList()
        task_rv.adapter = taskAdapter
        task_rv.layoutManager = LinearLayoutManager(this)
        task_rv.hasFixedSize()
        completedCount = 0
        for (item in taskAdapter?.taskList!!) {
            if(item.complete)
                ++completedCount
        }

        runOnUiThread {
            progressBar.max = taskAdapter?.taskList?.size!!
            progressBar.progress = completedCount
            tv_percentage.text = "${getPercentage(progressBar.progress, progressBar.max)} %"
        }
    }

    override fun onTaskItemClicked(task: Task) {
        val intent = Intent(this, AddTaskActivity::class.java)
        intent.putExtra("tId", task.tId)
        intent.putExtra("title", task.title)
        intent.putExtra("priority", task.priority)
        intent.putExtra("detail", task.detail)
        intent.putExtra("complete", task.complete)
        startActivity(intent)
    }

    override fun onTaskItemLongClicked(task: Task) {
        val alertDialog = AlertDialog.Builder(this)
            .setItems(R.array.dialog_list, DialogInterface.OnClickListener { dialog, which ->
                if (which==0){
                    val intent = Intent(this@MainActivity, AddTaskActivity::class.java)
                    intent.putExtra("tId", task.tId)
                    intent.putExtra("title", task.title)
                    intent.putExtra("priority", task.priority)
                    intent.putExtra("detail", task.detail)
                    startActivity(intent)
                }else{
                    taskDatabase?.getTaskDao()?.removeTask(task)
                    onResume()
                }
                dialog.dismiss()
            })
            .create()
        alertDialog.show()
    }

    override fun onCheckBoxClicked(task: Task, position: Int) {
        var holder = task_rv.findViewHolderForAdapterPosition(position)
        runOnUiThread {
            //toggle
            if(task.complete){
                holder?.itemView?.checkBox_task_done1?.isChecked = false
                task.complete = false
            }else{
                holder?.itemView?.checkBox_task_done1?.isChecked = true
                task.complete = true
            }
            taskDatabase!!.getTaskDao().updateTask(task)
            if(task.complete){
                ++completedCount
            }else{
                --completedCount
            }

            progressBar.max = taskAdapter?.taskList?.size!!
            progressBar.progress = completedCount
            tv_percentage.text = "${getPercentage(progressBar.progress, progressBar.max)} %"

        }
    }

    fun getPercentage(num : Int, denom : Int) : Int{
        return ((num.toFloat()/denom.toFloat())*100).toInt()
    }
}