package com.nattysoft.nathi_task

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.nattysoft.nathi_todo.R
import com.nattysoft.nathi_todo.data.local.models.Task
import java.util.ArrayList

class TaskAdapter(var taskList: List<Task>? = ArrayList<Task>()): RecyclerView.Adapter<TaskAdapter.TaskViewHolder>(){

    private var onTaskItemClickedListener: OnTaskItemClickedListener?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val layout = if (itemCount == 0) R.layout.empty_view else R.layout.task_item_view
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return TaskViewHolder(view, taskList!!)
    }

    override fun getItemCount(): Int {
        return if(taskList!!.isEmpty()) 0 else taskList!!.size
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int){
        holder.view.setOnClickListener{onTaskItemClickedListener!!.onTaskItemClicked(taskList!!.get(position))}
        holder.view.setOnLongClickListener{
            onTaskItemClickedListener!!.onTaskItemLongClicked(taskList!!.get(position))
            true
        }
        var doneCheckbox = holder.view.findViewById<CheckBox>(R.id.checkBox_task_done1)
        doneCheckbox.setOnClickListener{onTaskItemClickedListener!!.onCheckBoxClicked(taskList!!.get(position), position)}
        holder.onBindViews(position)
    }

    inner class TaskViewHolder(val view: View, val taskList: List<Task>): RecyclerView.ViewHolder(view){

        fun onBindViews(position: Int){
            if (itemCount != 0){
                view.findViewById<TextView>(R.id.title).text = taskList.get(position).title
                view.findViewById<TextView>(R.id.first_letter).text = (position+1).toString()+"."
                view.findViewById<TextView>(R.id.details).text = taskList.get(position).detail
                view.findViewById<ImageView>(R.id.priority_imgView).setImageResource(getImage(taskList.get(position).priority))
                view.findViewById<CheckBox>(R.id.checkBox_task_done1).isChecked = taskList.get(position).complete
            }

        }
        private fun getImage(priority: Int): Int
                = if (priority == 1) R.drawable.low_priority else if(priority == 2) R.drawable.medium_priority else R.drawable.high_priority
    }

    fun setTaskItemClickedListener(onTaskItemClickedListener: OnTaskItemClickedListener){
        this.onTaskItemClickedListener = onTaskItemClickedListener
    }

    interface OnTaskItemClickedListener{
        fun onTaskItemClicked(task: Task)
        fun onTaskItemLongClicked(task: Task)
        fun onCheckBoxClicked(task: Task, position: Int)
    }
}