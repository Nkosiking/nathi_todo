package com.nattysoft.nathi_todo.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverter
import android.content.Context
import com.nattysoft.nathi_todo.data.local.models.Task

//You can also check out type converters
@Database(entities = [Task::class], version = 1, exportSchema = false)
abstract class TaskListDatabase: RoomDatabase(){

    /**
     * This is an abstract method that returns a dao for the Db
     * */
    abstract fun getTaskDao(): TaskDao

    /**
     * A singleton design pattern is used to ensure that the database instance created is one
     * */
    companion object {
        val databaseName = "taskdatabase"
        var taskListDatabase: TaskListDatabase? = null

        fun getInstance(context: Context): TaskListDatabase?{
            if (taskListDatabase == null){
                taskListDatabase = Room.databaseBuilder(context,
                    TaskListDatabase::class.java,
                    TaskListDatabase.databaseName)
                    .allowMainThreadQueries()//i will remove this later, database are not supposed to be called on main thread
                    .build()
            }
            return taskListDatabase
        }
    }
}