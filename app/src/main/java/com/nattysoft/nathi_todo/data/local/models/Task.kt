package com.nattysoft.nathi_todo.data.local.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "task")
class Task(
    @ColumnInfo(name = "task_title")
    var title:String = "",
    @ColumnInfo(name = "task_priority")
    var priority: Int = 0,
    @PrimaryKey(autoGenerate = true) var tId: Int = 0){
    var detail: String = ""
    var complete: Boolean = false
}