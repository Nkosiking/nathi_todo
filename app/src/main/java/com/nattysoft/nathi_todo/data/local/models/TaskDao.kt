package com.nattysoft.nathi_todo.data.local

import android.arch.persistence.room.*
import com.nattysoft.nathi_todo.data.local.models.Task

@Dao
interface TaskDao{

    /**
     * SELECT -> This retrieve rows from a table in a database
     * FROM -> You specify the table to retrieve the rows from
     * ORDER BY -> This is just a sort algorithm
     * ASC -> Ascending order
     * WHERE -> This is a condition used to query data
     * */
    @Query("SELECT*FROM task ORDER BY tId ASC")
    fun getTaskList(): List<Task>


    @Query("SELECT*FROM task WHERE tId=:tid")
    fun getTaskItem(tid: Int): Task
    /**
     * @param task is what we want to save in our database
     * so many conflict can occur when a data is to be saved, the strategy is used to handle such conflicts
     * Abort -> this aborts the transaction
     * Ignore -> this ignores and continues the transaction
     * Replace -> this replace the data
     * others includes fail, and roolback
     * */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveTask(task: Task)

    @Update
    fun updateTask(task: Task)

    @Delete
    fun removeTask(task: Task)
}