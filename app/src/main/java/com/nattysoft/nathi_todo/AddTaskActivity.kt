package com.nattysoft.nathi_todo

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.RadioGroup
import android.widget.Switch
import com.nattysoft.nathi_todo.R
import com.nattysoft.nathi_todo.data.local.TaskListDatabase
import com.nattysoft.nathi_todo.data.local.models.Task
import kotlinx.android.synthetic.main.activity_add_task.*

class AddTaskActivity : AppCompatActivity(), RadioGroup.OnCheckedChangeListener{

    private var taskDatabase: TaskListDatabase? = null
    private var priority = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        taskDatabase = TaskListDatabase.getInstance(this)
        radioGroup.setOnCheckedChangeListener(this)

        val title = intent.getStringExtra("title")
        if (title == null || title == ""){
            add_task.setOnClickListener{
                val task = Task(title_ed.text.toString(), priority)
                task.detail = detail_ed.text.toString()
                taskDatabase!!.getTaskDao().saveTask(task)
                finish()
            }
        }else{
            add_task.text = getString(R.string.update)
            val tId = intent.getIntExtra("tId", 0)
            title_ed.setText(title)
            detail_ed.setText(intent.getStringExtra("detail"))
            priority = intent.getIntExtra("priority",0)
            when (priority) {
                1 -> low.isChecked = true
                2 -> medium.isChecked = true
                3 -> high.isChecked = true

            }
            checkBox_task_done2.isChecked = intent.getBooleanExtra("complete", false)
            add_task.setOnClickListener {
                val task = Task(title_ed.text.toString(), priority, tId)
                task.detail = detail_ed.text.toString()
                task.complete = checkBox_task_done2.isChecked
                taskDatabase!!.getTaskDao().updateTask(task)
                finish()
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        if (checkedId == R.id.low){
            priority = 1
        }else if (checkedId == R.id.medium){
            priority = 2
        }else if (checkedId == R.id.high) {
            priority = 3
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            startActivity(Intent(this, MainActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

}